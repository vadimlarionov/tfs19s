# Настройка деплоя на сервер
Первоначально нам понадобится сервер. Часть команд по настройке сервера (без Gitlab Runner-а) можно найти в [server-setup.md](server-setup.md).

## Настройка базы
Для хранения состояния нашему приложения требуется БД. Самый простой способ её запустить и настроить - воспользоваться Docker Сompose. Мы выполним это в виде отдельного файла docker-compose.yml, чтобы БД существовала независимо от основного приложения. Так перезапуск приложения не затронет перезапуск БД.
Пример docker-compose.yml для БД: https://gitlab.com/vadimlarionov/hello-app/blob/master/docker-compose-db.yml

1) Подключаемся к серверу по ssh;
2) Создаём директорию db и переходим в неё (`mkdir db && cd db`);
3) Создаём файл `docker-compose.yml` (`touch docker-compose.yml`);
4) Открываем файл `docker-compose.yml` и вставляем в него содержимое для настройки БД (`nano docker-compose.yml`, для сохранения - `CTRL+O`, для выхода - `CTRL+X`). Не забываем прописать в файл пароль для подключения к БД. Длина пароля - не менее 16 символов, запоминать его не потребуется, можно сгенерировать;
5) Запускаем БД и смотрим, чтобы в логе не было ошибок, чтобы БД запустилась (`docker-compose up`);
6) Если в логе ошибок не наблюдаем, то прерываем работу контейнеров (`CTRL+C`);
7) Запускаем БД в фоновом режиме `docker-compose up -d`.

Теперь у нас есть запущенная на сервере БД. DSN для подключения к ней выглядит следующим образом:
```
postgres://{{user}}:{{password}}@{{host}}:5432/{{db_name}}?sslmode=disable&fallback_application_name=my-app
```
Например,
```
postgres://myuser:mypassword@178.248.236.218:5432/my_db?sslmode=disable&fallback_application_name=my-app
```

Теперь подключимся к БД и создадим необходимые таблицы:
1) Найдём id запущенного контейнера с БД (`docker ps | grep postgres`)
2) id располагается в первой колонке. Скопируем его (например, `092546d79ddc`)
3) Подключаемся к контейнеру (`docker exec -it 092546d79ddc /bin/bash`)
4) В контейнере подключаемся к postgresql (`psql -Uuser my_db`), где `user` - имя пользователя, `my_db` - имя базы данных;
5) Вставляем команды для создания таблиц БД. Список таблиц можно посмотреть с помощью команды (`\d`);
6) Выходим из psql (`CTRL+D`), контейнера (`CTRL+D`).

Чтобы проверить, что ваша БД доступна из-вне, можно подключиться к ней с локальной машины (например, запустить ваше приложение либо подключиться через Goland из вклюдки `databases`).

## Конфигурирование приложения и .gitlab-ci.yml
Наше приложение необходимо сконфигурировать. Например, указать параметры подключения к БД.
Для этого вынесем все параматры в файл в env-переменными и укажем в docker-compose.yml путь до файла с ними. Теперь необходимо при старте приложения вычитывать env-переменные и инициализировать конфиг.
Путь для подключения к базе (DSN) в репозитории **не** храним, т.к. он содержит пароль для подключения к БД.
DSN маскируем в base64 (т.к. Gitlab не позволяет сохранить его напрямую) и добавляем в environment-переменную Gitlab.
![DB_URL environment variable](img/db_url_env.png)

В приложении будем декодировать base64-строку и извлекать DSN.

В docker-compose.yml создадим общую сеть между приложениями.
Это позволит приложениям обращаться друг к другу по имени сервиса (из примера `gateway` подключается к `hello-api` по адресу `hello-api:9000`), 
где `hello-api` - имя сервиса, `9000` - порт, на котором работает микросервис hello-api. Параметры подключения указываем в env-файле ((пример)[https://gitlab.com/vadimlarionov/hello-app/blob/master/envs/gateway/dev#L2]).

## Ссылки с примерами кода
* Репозиторий: https://gitlab.com/vadimlarionov/hello-app
* Пример конфигирирования: https://gitlab.com/vadimlarionov/hello-app/blob/master/cmd/gateway/main.go#L22-64
* env-переменные: https://gitlab.com/vadimlarionov/hello-app/blob/master/envs/gateway/dev
* docker-compose.yml: https://gitlab.com/vadimlarionov/hello-app/blob/master/docker-compose.yml
* docker-compose-db.yml: https://gitlab.com/vadimlarionov/hello-app/blob/master/docker-compose-db.yml

