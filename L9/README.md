# Инструкции по настройке сервера и CI/CD

Настройка сервера и Gitlab Runner-а: [server-setup.md](server-setup.md)   
Настройка БД и деплой: [deploy.md](deploy.md)
