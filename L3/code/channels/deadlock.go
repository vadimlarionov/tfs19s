package main

import "fmt"

func main() {
	dataCh := make(chan string)
	go func() {
		if true {
			return
		}
		dataCh <- "Hello channel!"
	}()
	fmt.Println(<-dataCh)
}
