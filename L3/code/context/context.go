package main

import (
	"context"
	"fmt"
	"time"
)

func main() {
	duration := 150 * time.Millisecond
	ctx, cancel := context.WithTimeout(context.Background(), duration)
	defer cancel()

	ch := make(chan int, 1)
	go func() {
		//time.Sleep(50 * time.Millisecond)
		time.Sleep(500 * time.Millisecond)
		ch <- 123
	}()

	select {
	case d := <-ch:
		fmt.Printf("completed, result: %d\n", d)
	case <-ctx.Done():
		fmt.Println("cancelled")
	}
}
