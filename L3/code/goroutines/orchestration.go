package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	runtime.GOMAXPROCS(2)

	var wg sync.WaitGroup
	wg.Add(2)

	fmt.Println("Starting...")

	go printABC('A', &wg)
	go printABC('a', &wg)

	wg.Wait()
	fmt.Println("\nFinished")
}
func printABC(firstLetter rune, wg *sync.WaitGroup) {
	defer wg.Done()
	for i := 0; i < 10; i++ {
		for char := firstLetter; char < firstLetter+26; char++ {
			fmt.Printf("%c ", char)
		}
	}
}
