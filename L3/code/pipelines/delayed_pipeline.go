package main

import (
	"fmt"
	"time"
)

func gen(start <-chan struct{}, nums ...int) <-chan int {
	out := make(chan int)
	go func() {
		<-start
		for _, n := range nums {
			out <- n
		}
		close(out)
	}()
	return out
}

func sq(in <-chan int) <-chan int {
	out := make(chan int)
	go func() {
		for n := range in {
			out <- n * n
		}
		close(out)
	}()
	return out
}

func printResult(in <-chan int) <-chan struct{} {
	out := make(chan struct{})
	go func() {
		// Consume the output.
		for res := range in {
			fmt.Println(res) // 4
		}
		close(out)
	}()
	return out
}

func main() {
	// Set up the pipeline.
	start := make(chan struct{})
	c := gen(start, 2, 3, 4, 5)
	out := sq(c)
	finish := printResult(out)

	// we can do something or wait before pipeline starts working
	time.Sleep(1 * time.Second)

	start <- struct{}{}

	<-finish
}
