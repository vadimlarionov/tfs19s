package main

import (
	"log"
	"net/http"

	"github.com/go-chi/chi"
)

func GetGreeting(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello, anonymous!"))
}

func PostGreeting(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Greetings broadcasted!"))
}

func main() {
	r := chi.NewRouter()
	r.Route("/", func(r chi.Router) {
		r.Get("/greeting", GetGreeting)
		r.Post("/greeting", PostGreeting)
	})

	if err := http.ListenAndServe(":5000", r); err != nil {
		log.Fatal(err)
	}
}
