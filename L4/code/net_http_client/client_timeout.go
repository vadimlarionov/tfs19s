package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/golang/go/src/log"
)

func main() {
	client := &http.Client{
		Timeout: time.Millisecond * 50,
	}
	req, err := http.NewRequest("GET", "https://postman-echo.com/get", nil)
	if err != nil {
		log.Fatal(err)
	}

	resp, err := client.Do(req)
	// always handle errors
	/**	if err != nil {
		log.Fatal(err)
	}**/
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s", body)
}
