package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/golang/go/src/log"
)

func main() {
	postData := strings.NewReader(`{"name":"Seva"}`)
	resp, err := http.Post("https://postman-echo.com/post", "application/json", postData)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%s", body)
}
