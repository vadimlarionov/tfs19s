package main

import (
	"fmt"
	"sort"
	"strconv"
)

func Sequence() func() int {
	i := 0
	return func() int {
		i++
		return i
	}
}

func main() {
	seq := Sequence()
	fmt.Println(seq())
	fmt.Println(seq())
	fmt.Println(seq())

	fmt.Println()
	seq1 := Sequence()
	fmt.Println(seq1())

	fmt.Println()

	var str string
	appendToStr := func(val int) {
		str += " " + strconv.Itoa(val) // Функция обращается к переменной, объявленной вне функции
	}

	for i := 10; i < 100; i += 10 {
		appendToStr(i)
	}

	fmt.Printf("Str: %s\n", str)

	// Другой пример - сортировка
	numbers := []int{10, -5, 5, -3, 7, -1, 0, 2}
	sort.Slice(numbers, func(i, j int) bool {
		first := numbers[i]
		if first < 0 {
			first *= -1
		}

		second := numbers[j]
		if second < 0 {
			second *= -1
		}

		return first < second
	})
	fmt.Printf("Numbers: %+v", numbers)
}
