package main

import (
	"fmt"
	"time"
)

func DoHTTPRequest(url string) {
	// Логируем время выполнения запроса
	defer func(startedAt time.Time) {
		fmt.Printf("Execution time: %s\n", time.Since(startedAt))
	}(time.Now())

	fmt.Printf("Send request to %s\n", url)
	time.Sleep(time.Second)
	fmt.Printf("Success response from %s\n", url)
}

func PrintReverse() {
	for i := 0; i < 10; i++ {
		defer func(value int) {
			fmt.Printf("%d ", value)
		}(i)
	}
	fmt.Println("End PrintReverse")
}

func main() {
	DoHTTPRequest("https://golang.org")
	fmt.Println()

	PrintReverse()
}
