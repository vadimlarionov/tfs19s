package main

import (
	"fmt"
	"strconv"
)

// Функция без аргументов
func PrintHelloWorld() {
	fmt.Println("Hello world")
}

// Функия с одним параметром без возвращаемых значений
func PrintHello(name string) {
	fmt.Printf("Hello %s\n", name)
}

// Функция с одним принимаемым и одним возвращаемым значением
func MakeGreeting(name string) string {
	return fmt.Sprintf("Hello %s. Nice to meet you!", name)
}

// Функция с несколькими аргументами, повторяющимися по типу
func RegisterUser(firstName, lastName string, age int, weight, height float64) {
	fmt.Printf("Register user %s %s, age=%d, height=%f, weight=%f\n", firstName, lastName, age, weight, height)
}

// Функция с несколькими возвращаемыми значениями
func MinMax(slice []int) (int, int) {
	if len(slice) == 0 {
		return 0, 0
	}

	min := slice[0]
	max := slice[0]
	for _, elem := range slice {
		if elem < min {
			min = elem
		}

		if elem > max {
			max = elem
		}
	}

	return min, max
}

// Функция с именованными возвращаемыми значениями
// При return можно не перечислять возвращаемые значения
func MinMaxAvg(slice []int) (min, max int, avg float64) {
	if len(slice) == 0 {
		return
	}

	min, max = MinMax(slice)
	var sum int
	for _, elem := range slice {
		sum += elem
	}

	avg = float64(sum) / float64(len(slice))
	return
	// return min, max, avg // Либо так
}

// Функция с переменным количеством аргументов
func PrintStudentRatings(firstName, lastName string, ratings ...int) {
	fmt.Printf("%s %s ratings: %+v", firstName, lastName, ratings)
}

// Функция, принимающая функцию для фильтрации
func PrintFiltered(filter func(v int) bool, values ...int) {
	var str string
	for _, val := range values {
		if filter(val) {
			str += " " + strconv.Itoa(val)
		}
	}

	fmt.Printf("PrintFiltered:%s\n", str)
}

func main() {
	PrintHelloWorld()

	PrintHello("John")

	greeting := MakeGreeting("John")
	fmt.Println(greeting)

	PrintStudentRatings("John", "Wick", 5, 5, 4, 5)

	min, max := MinMax([]int{5, 1, 4, 3, 2})
	fmt.Printf("MinMax: v1=%d, v2=%d\n", min, max)

	min, max, avg := MinMaxAvg([]int{2, 3, 4, 5})
	fmt.Printf("MinMaxAvg: min=%d, max=%d, avg=%f\n", min, max, avg)

	isEven := func(v int) bool {
		return v%2 == 0
	}

	PrintFiltered(isEven, 100, 23, 13, 18, 90, -10)
}
