package main

import "fmt"

// Проверяем на этапе компиляции, что робот удовлетворяет интерфейсу Mover
var _ Mover = &Robot{}

type Robot struct{}

func (r *Robot) Move(x, y int) {
	fmt.Printf("Move robot to (%d, %d)", x, y)
}

type Mover interface {
	Move(x, y int)
}

func MoveToStart(m Mover) {
	x, y := 10, 20 // start position
	m.Move(x, y)
}

func main() {
	robot := Robot{}
	MoveToStart(&robot)
}
