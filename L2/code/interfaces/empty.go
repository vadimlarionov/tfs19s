package main

import (
	"fmt"
	"io"
)

// Любой объект можно привести к пустому интерфейсу
func allIsEmptyInterface() {
	var variable interface{}
	variable = []int{1, 2, 3}
	fmt.Printf("%+v\n", variable)

	variable = "Hello world"
	fmt.Printf("%+v\n", variable)

	variable = func() int {
		return 100
	}
	fmt.Printf("%+v\n", variable)
}

func castEmptyInterface() {
	PrintValue("Hello")
	PrintValue(3)
	PrintValue(3.14)
	PrintValue(float32(3.14))
}

func PrintValue(val interface{}) {
	switch val.(type) {
	case string:
		fmt.Printf("Value is string: %q\n", val.(string))
	case int:
		fmt.Printf("Value is integer: %d\n", val.(int))
	case float64:
		fmt.Printf("Value is float64: %f\n", val.(float64))
	default:
		fmt.Printf("Unexpected type %T: %v\n", val, val)
	}
}

type User struct {
	Name string
}

func (u User) String() string {
	return fmt.Sprintf("User=%s", u.Name)
}

func exampleTypeAssertion() {
	var empty interface{}
	user := User{Name: "John"}

	empty = user
	stringer := empty.(fmt.Stringer)
	fmt.Printf("%s\n", stringer)

	// writer := empty.(io.Writer)
	// _ = writer // Чтобы программа компилировалась

	// Правильная обработка
	writer, ok := empty.(io.Writer)
	if !ok {
		fmt.Println("Can't cast empty struct to io.Writer")
		_ = writer
	}

	// Через switch
	switch empty.(type) {
	case fmt.Stringer:
		fmt.Printf("Cast empty to fmt.Stringer: %s\n", empty.(fmt.Stringer))
		// Ниже можно попробовать привести к другим типам, но выполнится только 1 case
	}
}

func main() {
	allIsEmptyInterface()

	castEmptyInterface()

	exampleTypeAssertion()
}
