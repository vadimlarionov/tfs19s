package main

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"
)

const dateLayout = "2006-01-02"

type Date struct {
	time.Time
}

// Структура удовлетворяет интерфейсу json.Marshaler https://golang.org/pkg/encoding/json/#Marshaler
func (t Date) MarshalJSON() ([]byte, error) {
	data := make([]byte, 0, 2+len(dateLayout))
	data = append(data, '"')
	data = t.AppendFormat(data, dateLayout)
	data = append(data, '"')
	return data, nil
}

// Структура удовлетворяет интерфейсу json.Unmarshaler https://golang.org/pkg/encoding/json/#Unmarshaler
func (t *Date) UnmarshalJSON(data []byte) error {
	trimmed := strings.Trim(string(data), `"`)
	parsed, err := time.Parse(dateLayout, trimmed)
	if err != nil {
		return fmt.Errorf("can't parse time: %s", err)
	}
	t.Time = parsed

	return nil
}

type Student struct {
	Name     string `json:"name"`
	about    string
	Birthday Date `json:"birthday"`
}

func main() {
	student1 := Student{
		Name:     "Keira Christina Knightley",
		about:    "I'm actress",
		Birthday: Date{Time: time.Date(1985, time.March, 26, 0, 0, 0, 0, time.UTC)},
	}

	b, err := json.Marshal(student1)
	if err != nil {
		log.Fatalf("Can't marshal student: %s\n", err)
	}
	fmt.Printf("Student1: %s\n", b)

	data := `{"name":"William Bradley Pitt","birthday":"1963-12-18","about":"I'm actor'"}`
	var student2 Student
	if err = json.Unmarshal([]byte(data), &student2); err != nil {
		log.Fatalf("Can't unmarshal data: %s\n", err)
	}

	fmt.Printf("Student2: %+v\n", student2)
}
