package main

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/gorilla/websocket"
	"html/template"
	"log"
	"net/http"
	"strconv"
	"sync"
)

type Lot struct {
	ID         int
	Title      string
	StartPrice float64
}

var lots = []Lot{
	{0, "Наручные часы", 100},
	{1, "Карманные часы", 110},
	{2, "Настенные часы", 300},
}

var templates map[string]*template.Template

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type PriceTick struct {
	LotID int
	Price float64
}

var priceTickCh = make(chan PriceTick)

func init() {
	if templates == nil {
		templates = make(map[string]*template.Template)
	}
	templates["index"] = template.Must(template.ParseFiles("html/index.html", "html/base.html"))
}

func renderTemplate(w http.ResponseWriter, name string, template string, viewModel interface{}) {
	tmpl, ok := templates[name]
	if !ok {
		http.Error(w, "can't find template", http.StatusInternalServerError)
	}
	err := tmpl.ExecuteTemplate(w, template, viewModel)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func GetLots(w http.ResponseWriter, r *http.Request) {
	renderTemplate(w, "index", "base", lots)
}

func PostUpdateLotPrice(w http.ResponseWriter, r *http.Request) {
	lotID, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		http.Error(w, "can't find lot by ID", http.StatusBadRequest)
		return
	}

	if lotID > len(lots) {
		http.Error(w, "can't find lot by ID", http.StatusBadRequest)
		return
	}

	newPrice, err := strconv.ParseFloat(r.PostFormValue("newPrice"), 64)
	if err != nil {
		http.Error(w, "can't find new price", http.StatusBadRequest)
		return
	}

	lots[lotID].StartPrice += newPrice
	w.Write([]byte(fmt.Sprintf("%.2f", lots[lotID].StartPrice)))
	priceTickCh <- PriceTick{lotID, lots[lotID].StartPrice}
}

type WSClients struct {
	wsConn []*websocket.Conn
	sync.Mutex
}

func (clients *WSClients) AddClient(conn *websocket.Conn) {
	clients.Mutex.Lock()
	clients.wsConn = append(clients.wsConn, conn)
	clients.Mutex.Unlock()
	fmt.Printf("added client, total clients: %d\n", len(wsClients.wsConn))
}

func (clients *WSClients) removeClientByID(id int) {
	clients.Mutex.Lock()
	clients.wsConn = append(clients.wsConn[:id], clients.wsConn[id+1:]...)
	clients.Mutex.Unlock()
	fmt.Printf("removed client #%d, total clients %d\n", id, len(clients.wsConn))
}

func (clients *WSClients) BroadcastMessage(message []byte) {
	for i, c := range clients.wsConn {
		err := c.WriteMessage(websocket.TextMessage, message)
		if err != nil {
			fmt.Printf("can't broadcast message: %+v\n", err)
			clients.removeClientByID(i)
		}
	}
}

var wsClients WSClients


func WSPriceUpdate(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Printf("can't upgrade connection: %s\n", err)
		return
	}

	wsClients.AddClient(conn)

	for {
		priceTick := <- priceTickCh
		res, err := json.Marshal(priceTick)
		if err != nil {
			fmt.Printf("can't marshal message: %+v\n", err)
			continue
		}
		wsClients.BroadcastMessage(res)
	}
	conn.Close()
}

func main() {
	r := chi.NewRouter()

	r.Route("/auction", func(r chi.Router) {
		r.Get("/", GetLots)
		r.Post("/lots/{id}/updprice", PostUpdateLotPrice)
		r.HandleFunc("/ws", WSPriceUpdate)
	})

	if err := http.ListenAndServe(":5000", r); err != nil {
		log.Fatal(err)
	}
}
