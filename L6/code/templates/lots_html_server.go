package main

import (
	"github.com/go-chi/chi"
	"html/template"
	"log"
	"net/http"
	"strconv"
)

type Lot struct {
	ID         int
	Title      string
	StartPrice float64
}

var lots = []Lot{
	{0, "Наручные часы", 100},
	{1, "Карманные часы", 110},
	{2, "Настенные часы", 300},
}

var templates map[string]*template.Template

func init() {
	if templates == nil {
		templates = make(map[string]*template.Template)
	}
	templates["index"] = template.Must(template.ParseFiles("html/index.html", "html/base.html"))
	templates["updprice"] = template.Must(template.ParseFiles("html/updprice.html", "html/base.html"))
}

func renderTemplate(w http.ResponseWriter, name string, template string, viewModel interface{}) {
	tmpl, ok := templates[name]
	if !ok {
		http.Error(w, "can't find template", http.StatusInternalServerError)
	}
	err := tmpl.ExecuteTemplate(w, template, viewModel)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func GetLots(w http.ResponseWriter, r *http.Request) {
	renderTemplate(w, "index", "base", lots)
}

func GetUpdateLotPrice(w http.ResponseWriter, r *http.Request) {
	lotID, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		http.Error(w, "can't find lot by ID", http.StatusBadRequest)
		return
	}

	if lotID > len(lots) {
		http.Error(w, "can't find lot by ID", http.StatusBadRequest)
		return
	}
	lot := lots[lotID]

	renderTemplate(w, "updprice", "base", lot)
}

func PostUpdateLotPrice(w http.ResponseWriter, r *http.Request) {
	lotID, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		http.Error(w, "can't find lot by ID", http.StatusBadRequest)
		return
	}

	if lotID > len(lots) {
		http.Error(w, "can't find lot by ID", http.StatusBadRequest)
		return
	}

	newPrice, err := strconv.ParseFloat(r.PostFormValue("newPrice"), 64)
	if err != nil {
		http.Error(w, "can't find new price", http.StatusBadRequest)
		return
	}

	lots[lotID].StartPrice = newPrice

	http.Redirect(w, r, "/auction", 302)
}

func main() {
	r := chi.NewRouter()

	r.Route("/auction", func(r chi.Router) {
		r.Get("/", GetLots)
		r.Get("/lots/{id}/updprice", GetUpdateLotPrice)
		r.Post("/lots/{id}/updprice", PostUpdateLotPrice)
	})

	if err := http.ListenAndServe(":5000", r); err != nil {
		log.Fatal(err)
	}
}
