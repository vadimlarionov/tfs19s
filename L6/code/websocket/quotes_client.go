package main

import (
	"fmt"
	"golang.org/x/net/websocket"
	"io"
	"log"
)

func main() {
	url := "ws://localhost:5000/stocks"
	origin := "http://localhost:5000/stocks"
	conn, err := websocket.Dial(url, "", origin)
	if err != nil {
		log.Fatalf("can't connect to server: %s", err)
	}
	var msg string
	for {
		err := websocket.Message.Receive(conn, &msg)
		if err != nil {
			if err == io.EOF {
				break
			}
			fmt.Printf("can't receive: %s\n", err)
			break
		}
		fmt.Printf("received from server: %s\n", msg)
		// return the msg
		err = websocket.Message.Send(conn, msg)
		if err != nil {
			fmt.Printf("can't send msg: %s", err)
			break
		}
	}
}
