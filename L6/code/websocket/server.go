package main

import (
	"fmt"
	"golang.org/x/net/websocket"
	"log"
	"net/http"
)

func WSHandler(ws *websocket.Conn) {
	fmt.Println("web socket handler function")
}

func main() {
	http.Handle("/stocks", websocket.Handler(WSHandler))
	err := http.ListenAndServe(":5000", nil)
	if err != nil {
		log.Fatal(err)
	}
}
