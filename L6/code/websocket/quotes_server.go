package main

import (
	"fmt"
	"golang.org/x/net/websocket"
	"log"
	"net/http"
)

func Quotes(ws *websocket.Conn) {
	for n := 0; n < 10; n++ {
		msg := "price " + string(n+48)
		fmt.Println("sending to client: " + msg)
		err := websocket.Message.Send(ws, msg)
		if err != nil {
			fmt.Printf("can't send: %s\n", err)
			break
		}
		var reply string
		err = websocket.Message.Receive(ws, &reply)
		if err != nil {
			fmt.Printf("can't receive: %s\n", err)
			break
		}
		fmt.Printf("received from client: %s\n", reply)
	}
}

func main() {
	http.Handle("/stocks", websocket.Handler(Quotes))
	err := http.ListenAndServe(":5000", nil)
	if err != nil {
		log.Fatalf("can't start server: %s", err)
	}
}
