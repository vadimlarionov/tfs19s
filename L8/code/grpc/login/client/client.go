package main

import (
	"context"
	"fmt"
	"log"
	"tfs19s/L8/code/grpc/login/loginpb"

	"google.golang.org/grpc"
)

func main() {
	conn, err := grpc.Dial("localhost:5000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("can't connect to server: %v", err)
	}
	defer conn.Close()
	client := loginpb.NewLoginServiceClient(conn)

	req := loginpb.LoginRequest{
		Login:    "login",
		Password: "passwd",
	}
	resp, err := client.Login(context.Background(), &req)
	if err != nil {
		log.Fatalf("can't login: %v", err)
	}
	fmt.Printf("response %+v", resp)
}
