#!/bin/bash
protoc ./greet/greetpb/GreetService.proto --go_out=plugins=grpc:.
protoc ./login/loginpb/LoginService.proto --go_out=plugins=grpc:.
protoc ./lots/lotspb/LotsService.proto --go_out=plugins=grpc:.
protoc ./orders/orderspb/OrdersService.proto --go_out=plugins=grpc:.