package main

import (
	"log"
	"net"
	"tfs19s/L8/code/grpc/greet/greetpb"

	"google.golang.org/grpc"
)

type server struct{}

func main() {
	listen, err := net.Listen("tcp", ":5000")
	if err != nil {
		log.Fatalf("can't listen on port: %v", err)
	}

	s := grpc.NewServer()
	greetpb.RegisterGreetServiceServer(s, server{})
	if err := s.Serve(listen); err != nil {
		log.Fatalf("can't register service server: %v", err)
	}
}
