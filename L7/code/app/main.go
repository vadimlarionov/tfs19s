package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	_ "net/http/pprof"
)

type News struct {
	ID          int64     `json:"id"`
	Title       string    `json:"title"`
	Body        string    `json:"body"`
	PublishedAt time.Time `json:"published_at"`
	Provider    string    `json:"provider"`
	Tickers     []string  `json:"tickers"`
}

func main() {
	var fileName string
	var templateFile string
	flag.StringVar(&fileName, "filename", "news.json", "File name with news.json")
	flag.StringVar(&templateFile, "template", "index.html", "Path to index.html")
	flag.Parse()

	news, err := ReadNews(fileName)
	if err != nil {
		log.Fatalf("Can't read news: %s", err)
	}

	handler, err := NewHandler(news, templateFile)
	if err != nil {
		log.Fatalf("Can't create handler: %s", err)
	}

	http.HandleFunc("/", handler.FindNews)

	fmt.Printf("Starting server on http://localhost:8080\n")
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal("Can't listen and serve:", err)
	}
}

func ReadNews(fileName string) ([]News, error) {
	data, err := ioutil.ReadFile(fileName)
	if err != nil {
		return nil, fmt.Errorf("can't read file: %s", err)
	}

	var news []News
	if err = json.Unmarshal(data, &news); err != nil {
		return nil, fmt.Errorf("can't unmarshal news: %s", err)
	}

	return news, nil
}

type Handler struct {
	tmpl *template.Template
	news []News
}

func NewHandler(news []News, templateFile string) (*Handler, error) {
	tmpl, err := template.New("index.html").ParseFiles(templateFile)
	if err != nil {
		return nil, fmt.Errorf("can't parse template: %s", err)
	}

	return &Handler{
		tmpl: tmpl,
		news: news,
	}, nil
}

func (h *Handler) FindNews(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	fmt.Println("Process request", r.URL.RawPath)

	var news []News
	q := r.URL.Query()
	query := q.Get("q")

	limitArg := q.Get("limit")
	var limit int
	var err error
	if limitArg != "" {
		limit, err = strconv.Atoi(limitArg)
		if err != nil {
			limit = maxLimit
		}
	}

	if query != "" {
		var err error
		news, err = h.findNews(query, limit)
		if err != nil {
			fmt.Printf("Can't find news: %s\n", err)
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, "Internal error")
			return
		}
	}

	var buf bytes.Buffer
	if err := h.tmpl.Execute(&buf, map[string]interface{}{"News": news}); err != nil {
		fmt.Printf("Can't execute template: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Internal error")
		return
	}

	w.Write(buf.Bytes())
}

const maxLimit = 100000

func (h *Handler) findByRegexp(query string, limit int) ([]News, error) {
	if limit <= 0 {
		limit = maxLimit
	}

	r, err := regexp.Compile(query)
	if err != nil {
		return nil, fmt.Errorf("invalid query expression: %s", err)
	}

	var result []News
	var count int
	for _, n := range h.news {
		if r.MatchString(n.Title) || r.MatchString(n.Body) {
			result = append(result, n)
			count++
		}

		if count >= limit {
			break
		}
	}

	return result, nil
}

func (h *Handler) findNews(query string, limit int) ([]News, error) {
	if limit <= 0 {
		limit = maxLimit
	}

	var result []News

	var count int
	fmt.Println(query)
	for _, n := range h.news {
		if strings.Contains(strings.ToUpper(n.Title), strings.ToUpper(query)) ||
			strings.Contains(strings.ToUpper(n.Body), strings.ToUpper(query)) {

			result = append(result, n)
			count++
		}

		if count >= limit {
			break
		}
	}

	return result, nil
}
