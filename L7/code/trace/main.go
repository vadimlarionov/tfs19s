package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"runtime/trace"
	"strings"
)

func main() {
	var traceFile string
	var mode string
	var iterations int64
	flag.StringVar(&traceFile, "trace", "trace.out", "File name to write trace results")
	flag.StringVar(&mode, "mode", "concat", "String concatenation mode: concat|builder")
	flag.Int64Var(&iterations, "n", 100000, "Number of iterations")

	if traceFile != "" {
		f, err := os.Create(traceFile)
		if err != nil {
			log.Fatal("can't create trace output file: ", err)
		}
		defer f.Close()

		if err = trace.Start(f); err != nil {
			log.Fatal("can't start trace: ", err)
		}
		defer trace.Stop()
	}

	switch mode {
	case "concat":
		concatMode(iterations)
	case "builder":
		builderMode(iterations)
	case "repeat":
		repeatMode(iterations)
	default:
		log.Fatalf("Unknown type %q", mode)
	}
}

const str = "Hello!"

func concatMode(n int64) {
	var result string
	for i := int64(0); i < n; i++ {
		result += str
	}
	fmt.Println(result)

	//fmt.Fprint(ioutil.Discard, result)
}

func builderMode(n int64) {
	builder := &strings.Builder{}
	for i := int64(0); i < n; i++ {
		builder.WriteString(str)
	}

	fmt.Fprint(ioutil.Discard, builder.String())
}

func repeatMode(n int64) {
	result := strings.Repeat(str, int(n))
	fmt.Fprint(ioutil.Discard, result)
}
