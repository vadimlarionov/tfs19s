package primes

import (
	"bytes"
	"strings"
	"testing"
)

const testStr = "Hello!"

func BenchmarkStringsConcat(b *testing.B) {
	var result string
	for i := 0; i < b.N; i++ {
		result += testStr
	}
}

func BenchmarkStringsBytesBuffer(b *testing.B) {
	buf := &bytes.Buffer{}
	for i := 0; i < b.N; i++ {
		buf.WriteString(testStr)
	}
}

func BenchmarkStringsBuilder(b *testing.B) {
	builder := &strings.Builder{}
	for i := 0; i < b.N; i++ {
		builder.WriteString(testStr)
	}
}

func BenchmarkStringsRepeat(b *testing.B) {
	strings.Repeat(testStr, b.N)
}
