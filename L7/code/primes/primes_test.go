package primes

import (
	"testing"
)

func TestIsPrime(t *testing.T) {
	type testCase struct {
		Name     string
		In       int
		Expected bool
	}

	testCases := []testCase{
		{Name: "Simple number", In: 7, Expected: true},
		{Name: "Zero value", In: 0, Expected: false},
		{Name: "Negative value", In: -100, Expected: false},
		{Name: "Non simple number", In: 100, Expected: false},
	}

	for _, tc := range testCases {
		t.Run(tc.Name, func(t *testing.T) {
			actual := IsPrimeNumber(tc.In)
			if actual != tc.Expected {
				t.Errorf("IsPrime(%d) = %+v, want %+v", tc.In, actual, tc.Expected)
			}
		})
	}
}

func BenchmarkIsPrimeNumber(b *testing.B) {
	bigPrimeNumber := 2002867943

	for i := 0; i < b.N; i++ {
		IsPrimeNumber(bigPrimeNumber)
	}
}
