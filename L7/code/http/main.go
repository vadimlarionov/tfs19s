package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {
	handler := &MetricHandler{
		storage: NewInMemoryStorage(),
	}

	http.HandleFunc("/", handler.Add)
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}
}

type Metric struct {
	Name  string `json:"name"`
	Count int64  `json:"count"`
}

// Storage

type MetricStorage interface {
	Add(name string, count int64) error
}

type InMemoryStorage struct {
	metrics map[string]int64
}

func NewInMemoryStorage() *InMemoryStorage {
	return &InMemoryStorage{
		metrics: make(map[string]int64),
	}
}

func (s *InMemoryStorage) Add(name string, count int64) error {
	s.metrics[name] += count
	return nil
}

// Handler

type MetricHandler struct {
	storage MetricStorage
}

func NewMetricHandler(storage MetricStorage) *MetricHandler {
	return &MetricHandler{storage: storage}
}

func (h *MetricHandler) Add(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		writeError(w, fmt.Errorf("can't read request body: %s", err))
		return
	}

	var metric Metric
	if err = json.Unmarshal(body, &metric); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		writeError(w, fmt.Errorf("can't unmarshal request body %q: %s", body, err))
		return
	}

	if err = h.storage.Add(metric.Name, metric.Count); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		writeError(w, fmt.Errorf("can't add metric %+v to storage: %s", metric, err))
		return
	}

	w.Write([]byte(`{"success": true}`))
}

type ErrorMessage struct {
	Err error `json:"error"`
}

func writeError(w http.ResponseWriter, err error) {
	msg := ErrorMessage{Err: err}
	data, err := json.Marshal(msg)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Printf("Can't marshal error: %s\n", err)
		return
	}

	w.Write(data)
}
